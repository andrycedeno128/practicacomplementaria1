//4.Recorrer el arreglo definido en la opción anterior 
//y mostrarlo aplicando 4 estructuras de repetición. 


const comidasFavoritas = [

   'Ceviche',
   'Encebollado',
   'Hamburguesas',
   'Papi pollo',
   'Coco'

];



muestramensaje(comidasFavoritas);


function muestramensaje(comidasFavoritas){

console.log('----------CICLOS---------');

ciclos(comidasFavoritas);

}


function ciclos(comidasFavoritas){
console.log('Ciclo for');
for (var i = 0; i < comidasFavoritas.length; i++) {
        	console.log(comidasFavoritas[i]);
        }

console.log('\nCiclo while');
var i = 0;
 while(i < comidasFavoritas.length){
     console.log(comidasFavoritas[i]);
     i++;
 }

 console.log('\nCiclo do while');
 var i = 0;
  do{
   console.log(comidasFavoritas[i]);
   i++;
  }while(i < comidasFavoritas.length);

 console.log('\nCiclo for in');
 for(var i in comidasFavoritas){
 	console.log(comidasFavoritas[i]);
 }


}